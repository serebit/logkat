import com.serebit.logkat.buildsrc.*

plugins {
    kotlin("multiplatform") version "1.4.0"
    id("org.jetbrains.dokka") version "1.4.0-dev-35"
    id("com.github.ben-manes.versions") version "0.29.0"
    `maven-publish`
}

group = "com.serebit.logkat"
version = System.getenv("SNAPSHOT_VERSION") ?: "0.6.1-dev"
description = "A lightweight and simple Kotlin logger"

repositories {
    mavenCentral()
    jcenter()
    maven("https://dl.bintray.com/kotlin/kotlin-dev")
}

kotlin {
    jvm {
        compilations["main"].kotlinOptions.useIR = true
        compilations["test"].apply {
            kotlinOptions.useIR = true

            defaultSourceSet.dependencies {
                implementation(kotlin("test-junit5"))
                implementation(group = "org.junit.jupiter", name = "junit-jupiter", version = "5.6.2")
            }
        }
    }

    sourceSets {
        commonTest.get().dependencies {
            implementation(kotlin("test-common"))
            implementation(kotlin("test-annotations-common"))
        }

        val linux64BitMain by creating { dependsOn(commonMain.get()) }
        setOf(linuxX64(), linuxArm64()).forEach {
            it.compilations["main"].defaultSourceSet.dependsOn(linux64BitMain)
        }

        val linux32BitMain by creating { dependsOn(commonMain.get()) }
        setOf(linuxArm32Hfp(), linuxMipsel32(), linuxMipsel32()).forEach {
            it.compilations["main"].defaultSourceSet.dependsOn(linux32BitMain)
        }

        all {
            languageSettings.useExperimentalAnnotation("kotlin.Experimental")
        }
    }
}

tasks.withType<Test>().configureEach { useJUnitPlatform() }

tasks.dokkaHtml {
    outputDirectory.set(rootDir.resolve("public/docs"))
}

publishing {
    createBintrayRepositories()

    val extraJavadocJar by jarTask()
    val extraSourcesJar by jarTask()

    publishing.publications.withType<MavenPublication>().all {
        // configure additional POM data for Maven Central
        configureForMavenCentral(extraJavadocJar, extraSourcesJar)
    }
}
