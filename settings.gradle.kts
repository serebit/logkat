rootProject.name = "logkat"

pluginManagement.repositories {
    gradlePluginPortal()
    maven("https://dl.bintray.com/kotlin/kotlin-dev")
}
